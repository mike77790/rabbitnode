
## Install
Add your files

cd chat

npm install

cp config.json.example config.json

1)copie el archivo config.json.example a config.json
2) Edite los parámetros "config.json" (utilice los suyos para el servidor RabbitMQ)

Configure su entorno local:


anfitrión virtual
carpeta "pública" como raíz del host virtual


##Uso

Ejecutar en bash node server.js

Abra la pestaña en el navegador con la URL de su servidor virtual (como http://chat.local?client_id=11)
Abra otra pestaña (como http://chat.local?client_id=12)(client_id cambiado)
Intente enviar un mensaje desde una pestaña y verá este mensaje en la segunda pestaña.
